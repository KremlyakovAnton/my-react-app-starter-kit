const express = require('express');
const webpack = require('webpack');
const path = require('path');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require("webpack-hot-middleware");

const app = express();
const config = require('./webpack/webpack.config.js');
const compiler = webpack(config);

let port = 3000;


app.get('/', (req, res)=> {
    res.sendFile('index.html', { root: path.join(__dirname, '/') })
});

app.use(
    webpackDevMiddleware(compiler, {
        hot: true,
        filename: "bundle.js",
        publicPath: "/",
        stats: {
            colors: true
        },
        historyApiFallback: true
    })
);

if (process.env.NODE_ENV == 'dev') {
    app.use(
        webpackHotMiddleware(compiler, {
            log: console.log,
            path: "/__webpack_hmr",
            heartbeat: 10 * 1000
        })
    );
}

app.listen(port, function () {
    console.log(`Example app listening on port ${port}!`);
});