import React from 'react';
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux';

import { store } from './store/configureStore';

import App from './containers/App/App';

if (module.hot) {
    module.hot.accept();
}

ReactDOM.render(
<Provider store={store}>
    <App />
</Provider>
, document.getElementById('react-app'));

